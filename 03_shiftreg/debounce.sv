/*-
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * Copyright (c) 2019 Rafal Kozik
 * All rights reserved.
 */

`default_nettype none

module debounce 
#(
	parameter N=8
) (
	input wire clk,
	input wire rst,
	input wire d,
	output logic q
);
	logic [N-1:0]stable_time;
	logic sr;
	logic dr;

	always_ff @(posedge clk or negedge rst)
		if (!rst)
			dr <= '0;
		else
			dr <= d;

	assign sr = (d != dr);

	always_ff @(posedge clk or negedge rst)
		if (!rst)
			stable_time <= '0;
		else if (sr)
			stable_time <= '0;
		else
			stable_time <= stable_time + 1;

	always_ff @(posedge clk or negedge rst)
		if (!rst)
			q <= '0;
		else if (stable_time[N-1])
			q <= dr;
endmodule

`default_nettype wire
