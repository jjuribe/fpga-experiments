/*-
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * Copyright (c) 2019 Rafal Kozik
 * All rights reserved.
 */

`default_nettype none

module shift_reg 
#(
	parameter N = 8
) (
	input wire clk,
	input wire rst,
	input wire ce,
	input wire d,
	output logic [N-1:0]q
);

	always_ff @(posedge clk or negedge rst)
		if (!rst)
			q <= '0;
		else if (ce) begin
			q[0] <= d;
			for (int i = 1; i < N; i++)
				q[i] <= q[i-1];
		end
endmodule

`default_nettype wire
