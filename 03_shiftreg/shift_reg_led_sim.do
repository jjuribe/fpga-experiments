#-
# SPDX-License-Identifier: BSD-3-Clause
#
# Copyright (c) 2019 Rafal Kozik
# All rights reserved.
#

vlib work

vlog shift_reg.sv
vlog debounce.sv
vlog edge_detector.sv
vlog shift_reg_led.sv
vlog shift_reg_led_tb.sv

vsim -novopt work.shift_reg_led_tb

add wave -position end sim:/shift_reg_led_tb/clk
add wave -position end sim:/shift_reg_led_tb/rst
add wave -position end sim:/shift_reg_led_tb/ce_in
add wave -position end  sim:/shift_reg_led_tb/dut/ce_db
add wave -position end  sim:/shift_reg_led_tb/dut/ce
add wave -position end sim:/shift_reg_led_tb/d_in
add wave -position end sim:/shift_reg_led_tb/dut/led

run 1000ms

wave zoom full
