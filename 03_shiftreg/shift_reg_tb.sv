/*-
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * Copyright (c) 2019 Rafal Kozik
 * All rights reserved.
 */

`default_nettype none
`timescale 100ps / 100ps

module shift_reg_tb; 
	logic clk, rst, ce, d;

	initial begin
		clk = 1'b0;
		forever #62.5ns clk = ~clk;
	end

	initial begin
		rst = 1'b0;
		#125ns rst = 1'b1;
	end

	initial begin
		ce = 1'b1;
		d = 1'b1;
		repeat (2) @(posedge clk);
		d = 1'b0;
		@(posedge clk);
		d = 1'b1;
		@(posedge clk);
		d = 1'b0;
		repeat (2) @(posedge clk);
		d = 1'b1;
		repeat (1) @(posedge clk);
		d = 1'b0;
		repeat (5) @(posedge clk);
		$stop;
	end

	shift_reg #(.N(4)) dut (
		.clk(clk),
		.rst(rst),
		.ce(ce),
		.d(d),
		.q()
	);

endmodule

`default_nettype wire
