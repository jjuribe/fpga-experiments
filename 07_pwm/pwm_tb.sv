/*-
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * Copyright (c) 2019 Rafal Kozik
 * All rights reserved.
 */

`default_nettype none
`timescale 100ps / 100ps

module pwm_tb;
	logic clk;
	logic rst;
	StreamBus bus(clk, rst);

	initial begin
		clk <= '0;
		forever #625 clk <= ~clk;
	end

	initial begin
		rst <= 1'b0;
		#1250 rst <= 1'b1;
	end

	initial begin
		bus.valid <= 1'b1;
		bus.data <= 4'd0;
		for (int i = 1; i <= 10; i++)
			#5us bus.data <= i;
		#5us $stop;
	end

	pwm #(.MAX(10)) dut (
		.bus(bus),
		.ce(1'b1),
		.pwm());

endmodule

`default_nettype wire
