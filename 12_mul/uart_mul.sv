/*-
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * Copyright (c) 2020 Rafal Kozik
 * All rights reserved.
 */

 `default_nettype none

module uart_mul #(
    parameter F = 8000000
) (
    input wire clk,
    input wire rst,
    input wire rx,
    output logic tx
);
    parameter N = 8;
    parameter BAUD = 115200;
    StreamBus #(.N(8)) bus_rx (.clk(clk), .rst(rst));
    StreamBus #(.N(8)) bus_tx (.clk(clk), .rst(rst));
    logic [7:0]data_in[3:0];
    logic cnt_in_ov;
    logic [3:0]valid_d;
    logic [15:0]c[1:0];
    logic [7:0]data_out[3:0];
    logic select_out;
    logic valid_out;
    logic [2:0]cnt_out;

    assign bus_rx.ready = 1'b1;
    uart_rx #(.F(F), .BAUD(BAUD)) urx (
        .rx(rx),
        .bus(bus_rx));

    always_ff @(posedge clk)
        if (bus_rx.valid) begin
            data_in[0] <= bus_rx.data;
            data_in[1] <= data_in[0];
            data_in[2] <= data_in[1];
            data_in[3] <= data_in[2];
        end

    counter #(.N(5)) cnt_in (
		.clk(clk),
		.rst(rst),
		.ce(bus_rx.valid | cnt_in_ov),
		.q(),
        .ov(cnt_in_ov));
    
    c_mul #(.K(8), .K_OUT1(0), .K_OUT2(15)) dut (
        .clk(clk),
        .rst(rst),
        .a_re(data_in[3]),
        .a_im(data_in[2]),
        .b_re(data_in[1]),
        .b_im(data_in[0]),
        .c_re(c[0]),
        .c_im(c[1]));

    always_ff @(posedge clk or negedge bus_rx.rst)
        if (!bus_rx.rst)
            valid_d <= '0;
        else
            valid_d <= {valid_d[2:0], cnt_in_ov};

    always_ff @(posedge clk)
        if (select_out | valid_out) begin
            data_out[0] <= c[1][15:8];
            data_out[1] <= select_out ? c[1][7:0] : data_out[0];
            data_out[2] <= select_out ? c[0][15:8] : data_out[1];
            data_out[3] <= select_out ? c[0][7:0] : data_out[2];
        end

    assign bus_tx.valid = valid_out;
    assign bus_tx.data = data_out[3];

    assign valid_out = cnt_out[2] & bus_tx.ready;
    assign select_out = valid_d[2]; 

    always_ff @(posedge clk or negedge bus_rx.rst)
        if (!bus_rx.rst)
            cnt_out <= 3'b000;
        else if (valid_d[3])
            cnt_out <= 3'b100;
        else if (valid_out)
            cnt_out <= cnt_out + 3'b1;

    uart_tx #(.F(F), .BAUD(115200)) utx (
        .bus(bus_tx),
        .tx(tx));

endmodule

`default_nettype wire
