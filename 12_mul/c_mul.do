#-
# SPDX-License-Identifier: BSD-3-Clause
#
# Copyright (c) 2020 Rafal Kozik
# All rights reserved.
#

vlib work

vlog c_mul.sv
vlog c_mul_tb.sv

vsim work.c_mul_tb

add wave -position end sim:/c_mul_tb/clk
add wave -position end sim:/c_mul_tb/rst
add wave -position end sim:/c_mul_tb/a_re
add wave -position end sim:/c_mul_tb/a_im
add wave -position end sim:/c_mul_tb/b_re
add wave -position end sim:/c_mul_tb/b_im
add wave -position end sim:/c_mul_tb/c_re
add wave -position end sim:/c_mul_tb/c_im

run -all
wave zoom full
