/*-
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * Copyright (c) 2019 Rafal Kozik
 * All rights reserved.
 */

`default_nettype none
`timescale 100ns / 10ns

module counter_tb;
	logic clk, rst, ce, ov1;

	initial begin
		clk <= '0;
		forever #5 clk <= ~clk;
	end

	initial begin
		rst <= 1'b0;
		#10 rst <= 1'b1;
	end

	initial begin
		ce <= 1'b1;
		#20 ce <= 1'b0;
		#20 ce <= 1'b1;
	end

	counter #(.N(10)) dut1 (
		.clk(clk),
		.rst(rst),
		.ce(ce),
		.q(),
		.ov(ov1)
	);

	counter #(.N(3)) dut2 (
		.clk(clk),
		.rst(rst),
		.ce(ce & ov1),
		.q(),
		.ov()
	);
endmodule

`default_nettype wire
