/*-
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * Copyright (c) 2020 Rafal Kozik
 * All rights reserved.
 */

 `default_nettype none
 `timescale 100ns / 10ns

 module adc_top_tb;
    logic clk, rst;

    initial begin
        clk <= '0;
        forever #62.5ns clk <= ~clk;
    end

    initial begin
        rst <= 1'b0;
        #1250 rst <= 1'b1;
    end

    adc_top dut(
        .clk(clk),
        .rst(rst),
        .led());

 endmodule

 `default_nettype wire
