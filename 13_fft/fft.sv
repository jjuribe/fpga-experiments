/*-
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * Copyright (c) 2020 Rafal Kozik
 * All rights reserved.
 */

`default_nettype none

module fft #(
	parameter K = 8,
	parameter N = 2,
	parameter LOG_N = $clog2(N)
) (
	input wire clk,
	input wire rst,
	input wire signed [K-1:0]d_in_re,
	input wire signed [K-1:0]d_in_im,
	input wire valid_in,
	output logic signed [K-1:0]d_out_re,
    output logic signed [K-1:0]d_out_im,
    output logic [LOG_N-1:0]addr,
	output logic valid_out
);
    logic signed [K-1:0]d_re[LOG_N:0];
    logic signed [K-1:0]d_im[LOG_N:0];
    logic valid[LOG_N:0];
    logic [LOG_N-1:0]addr1;

    assign d_re[0] = d_in_re;
    assign d_im[0] = d_in_im;
    assign valid[0] = valid_in;

    genvar i;
    generate
        for (i = 0; i < LOG_N; i++) begin : BUTTERFLY
            butterfly #(.K(K), .N(N / 2**i)) b (
                .clk(clk),
                .rst(rst),
                .d_in_re(d_re[i]),
                .d_in_im(d_im[i]),
                .valid_in(valid[i]),
                .d_out_re(d_re[i+1]),
                .d_out_im(d_im[i+1]),
                .valid_out(valid[i+1]));
        end
    endgenerate

    counter #(.N(N)) addr_cnt (
        .clk(clk),
        .rst(rst),
        .ce(valid[LOG_N]),
        .q(addr1),
        .ov());

    assign d_out_re = d_re[LOG_N];
    assign d_out_im = d_im[LOG_N];
    generate
        // assign addr = {<<{addr1}}; not supported
        for (i = 0; i < $bits(addr); i++) begin : BIT_REV
            assign addr[i] = addr1[$bits(addr) - i - 1];
        end
    endgenerate    
    assign valid_out = valid[LOG_N];

endmodule

`default_nettype wire
