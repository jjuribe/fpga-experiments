/*-
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * Copyright (c) 2020 Rafal Kozik
 * All rights reserved.
 */

`default_nettype none

module delay #(
	parameter N = 1,
	parameter L = 1
) (
    input wire clk,
    input wire rst,
    input wire ce,
	input wire [L-1:0]in,
    output logic [L-1:0]out
);
    logic [L-1:0]store[N-1:0];

	always_ff @(posedge clk or negedge rst)
        if (!rst)
            for (int i = 0; i < N; i++)
			    store[i] <= '0;
        else if (ce) begin
            store[0] <= in;
            for (int i = 1; i < N; i++)
                store[i] <= store[i-1];
        end

    assign out = store[N-1];

endmodule

`default_nettype wire
