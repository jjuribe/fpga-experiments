#-
# SPDX-License-Identifier: BSD-3-Clause
#
# Copyright (c) 2020 Rafal Kozik
# All rights reserved.
#

vlib work

vlog ../02_counter/counter.sv
vlog ../12_mul/c_mul.sv
vlog delay.sv
vlog twiddle_lut.sv
vlog butterfly.sv
vlog butterfly_tb.sv

vsim work.butterfly_tb

add wave -position end sim:/butterfly_tb/clk
add wave -position end sim:/butterfly_tb/rst

add wave -divider input
add wave -position end -decimal sim:/butterfly_tb/dut/d_in_re
add wave -position end -decimal sim:/butterfly_tb/dut/d_in_im
add wave -position end sim:/butterfly_tb/dut/valid_in

add wave -divider butterfly_in
add wave -position end -decimal sim:/butterfly_tb/dut/d1_re
add wave -position end -decimal sim:/butterfly_tb/dut/d1_im
add wave -position end sim:/butterfly_tb/dut/v1

add wave -position end -decimal sim:/butterfly_tb/dut/d2_re
add wave -position end -decimal sim:/butterfly_tb/dut/d2_im

add wave -divider butterfly_out
add wave -position end -decimal sim:/butterfly_tb/dut/add_re
add wave -position end -decimal sim:/butterfly_tb/dut/add_im

add wave -position end -decimal sim:/butterfly_tb/dut/sub_re
add wave -position end -decimal sim:/butterfly_tb/dut/sub_im

add wave -divider twiddle
add wave -position end -unsigned sim:/butterfly_tb/dut/i
add wave -position end -decimal sim:/butterfly_tb/dut/W_re
add wave -position end -decimal sim:/butterfly_tb/dut/W_im
add wave -position end -decimal sim:/butterfly_tb/dut/c_re
add wave -position end -decimal sim:/butterfly_tb/dut/c_im
add wave -position end sim:/butterfly_tb/dut/v2

add wave -divider select_out
add wave -position end -decimal sim:/butterfly_tb/dut/add1_re
add wave -position end -decimal sim:/butterfly_tb/dut/add1_im
add wave -position end -decimal sim:/butterfly_tb/dut/c1_re
add wave -position end -decimal sim:/butterfly_tb/dut/c1_im
add wave -position end -decimal sim:/butterfly_tb/dut/sel2

add wave -divider out
add wave -position end -decimal sim:/butterfly_tb/dut/sel1
add wave -position end sim:/butterfly_tb/dut/vs
add wave -position end sim:/butterfly_tb/dut/valid_out
add wave -position end -decimal sim:/butterfly_tb/dut/d_out_re
add wave -position end -decimal sim:/butterfly_tb/dut/d_out_im

run -all
wave zoom full
