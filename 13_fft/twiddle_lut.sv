/*-
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * Copyright (c) 2020 Rafal Kozik
 * All rights reserved.
 */

`default_nettype none

parameter real pi = 3.14159265358979323846;

module twiddle_lut #(
    parameter N = 2,
    parameter LOG_N = $clog2(N/2),
    parameter K = 9,
    parameter file_name = N
) (
    input wire clk,
    input wire [LOG_N-1:0]i,
    output logic signed [K-1:0]W_re,
    output logic signed [K-1:0]W_im
);
    logic [2*K-1:0]W[N/2-1:0];

    always_ff @(posedge clk)
        {W_re, W_im} <= W[i];

    initial begin
        // synthesis translate_off
        real MAX = (2**(K-1)-1.0);
        for (int i = 0; i < N/2; i++) begin
            real angle, rW_re, rW_im;
            logic signed [K-1:0] W_re;
            logic signed [K-1:0] W_im;
            angle = -2*pi*i/N;
            rW_re = 2**(K-1)*$cos(angle);
            rW_im = 2**(K-1)*$sin(angle);
            W_re = (rW_re > MAX) ? MAX : rW_re;
            W_im = (rW_im > MAX) ? MAX : rW_im;
            W[i] = {W_re, W_im};
        end
			case (N)
			4 : $writememb("04.mem", W);
			8 : $writememb("08.mem", W);
			16: $writememb("16.mem", W);
			32: $writememb("32.mem", W);
		  endcase
        // synthesis translate_on
		  case (N)
			4 : $readmemb("04.mem", W);
			8 : $readmemb("08.mem", W);
			16: $readmemb("16.mem", W);
			32: $readmemb("32.mem", W);
		  endcase
    end

endmodule

`default_nettype wire
