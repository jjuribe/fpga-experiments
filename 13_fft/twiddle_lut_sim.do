#-
# SPDX-License-Identifier: BSD-3-Clause
#
# Copyright (c) 2020 Rafal Kozik
# All rights reserved.
#

vlib work

vlog twiddle_lut.sv
vlog twiddle_lut_tb.sv

vsim work.twiddle_lut_tb

add wave -position end sim:/twiddle_lut_tb/clk
add wave -position end -unsigned sim:/twiddle_lut_tb/i
add wave -position end -decimal sim:/twiddle_lut_tb/dut/W_re
add wave -format analog-step -decimal -height 100 -min -256 -max 255 sim:/twiddle_lut_tb/dut/W_re
add wave -position end -decimal sim:/twiddle_lut_tb/dut/W_im
add wave -format analog-step -decimal -height 100 -min -256 -max 255 sim:/twiddle_lut_tb/dut/W_im

run -all
wave zoom full
