/*-
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * Copyright (c) 2020 Rafal Kozik
 * All rights reserved.
 */

`default_nettype none

module butterfly #(
	parameter K = 8,
	parameter N = 2,
	parameter LOG_N = $clog2(N)
) (
	input wire clk,
	input wire rst,
	input wire signed [K-1:0]d_in_re,
	input wire signed [K-1:0]d_in_im,
	input wire valid_in,
	output logic signed [K-1:0]d_out_re,
	output logic signed [K-1:0]d_out_im,
	output logic valid_out
);
	logic signed [K-1:0]d1_re;
	logic signed [K-1:0]d1_im;
	logic signed [K-1:0]d2_re;
	logic signed [K-1:0]d2_im;
	logic signed [K:0]add_re;
	logic signed [K:0]add_im;
	logic signed [K:0]add1_re;
	logic signed [K:0]add1_im;
	logic signed [K:0]sub_re;
	logic signed [K:0]sub_im;
	logic signed [K:0]W_re;
	logic signed [K:0]W_im;
	logic signed [K:0]c_re;
	logic signed [K:0]c_im;
	logic signed [K:0]c1_re;
	logic signed [K:0]c1_im;
	logic [LOG_N-1:0]i;
	logic v1, v2, vs;
	logic sel, sel1, sel2;

	always_ff @(posedge clk or negedge rst)
		if (!rst) v1 <= '0;
		else v1 <= valid_in;

	always_ff @(posedge clk)
		{d1_re, d1_im} <= {d_in_re, d_in_im};

	delay #(.N(N/2), .L(2*K)) d_d1 (
		.clk(clk),
		.rst(1'b1),
		.ce(v1),
		.in({d1_re, d1_im}),
		.out({d2_re, d2_im}));

	always_ff @(posedge clk) begin
		add_re <= d2_re + d1_re;
		add_im <= d2_im + d1_im;
		sub_re <= d2_re - d1_re;
		sub_im <= d2_im - d1_im;
	end

	delay #(.N(3), .L(2*(K+1))) d_add (
		.clk(clk),
		.rst(1'b1),
		.ce(1'b1),
		.in({add_re, add_im}),
		.out({add1_re, add1_im}));

	counter #(.N(N)) twiddle_cnt (
		.clk(clk),
		.rst(rst),
		.ce(v1),
		.q(i),
		.ov());

	generate
		if (N > 2) begin
			twiddle_lut #(.N(N), .K(K+1)) twiddle (
				.clk(clk),
				.i(i[LOG_N-2:0]),
				.W_re(W_re),
				.W_im(W_im));
		end else begin
			assign W_re = 2**K-1;
			assign W_im = '0;
		end
	endgenerate
	assign sel = i[LOG_N-1];

	c_mul #(.K(K+1)) mul (
		.clk(clk),
		.rst(rst),
		.a_re(sub_re),
		.a_im(sub_im),
		.b_re(W_re),
		.b_im(W_im),
		.c_re(c_re),
		.c_im(c_im));

	delay #(.N(4), .L(1)) d_v1 (
		.clk(clk),
		.rst(rst),
		.ce(1'b1),
		.in(v1),
		.out(v2));

	delay #(.N(N/2), .L(2*(K+1))) d_c (
		.clk(clk),
		.rst(1'b1),
		.ce(v2),
		.in({c_re, c_im}),
		.out({c1_re, c1_im}));

	delay #(.N(3), .L(1)) d_sel (
		.clk(clk),
		.rst(1'b1),
		.ce(1'b1),
		.in(sel),
		.out(sel1));

	always_ff @(posedge clk)
		sel2 <= sel1;

	always_ff @(posedge clk or negedge rst)
		if (!rst)
			vs <= 1'b0;
		else if (sel1)
			vs <= 1'b1;

	always_ff @(posedge clk)
		{d_out_re, d_out_im} <= sel2 ?
		{add1_re[K:1], add1_im[K:1]} :
		  {c1_re[K:1], c1_im[K:1]};

	always_ff @(posedge clk or negedge rst)
		if (!rst) valid_out <= 1'b0;
		else valid_out <= v2 & vs;

endmodule

`default_nettype wire
