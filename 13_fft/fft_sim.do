#-
# SPDX-License-Identifier: BSD-3-Clause
#
# Copyright (c) 2020 Rafal Kozik
# All rights reserved.
#

vlib work

vlog ../02_counter/counter.sv
vlog ../12_mul/c_mul.sv
vlog delay.sv
vlog twiddle_lut.sv
vlog butterfly.sv
vlog fft.sv
vlog fft_tb.sv

vsim work.fft_tb

add wave -position end sim:/fft_tb/clk
add wave -position end sim:/fft_tb/rst

add wave -divider input
add wave -position end -decimal sim:/fft_tb/dut/d_in_re
add wave -position end -decimal sim:/fft_tb/dut/d_in_im
add wave -position end sim:/fft_tb/dut/valid_in

add wave -divider middle
add wave -position end  sim:/fft_tb/dut/d_re
add wave -position end  sim:/fft_tb/dut/d_im
add wave -position end  sim:/fft_tb/dut/valid

add wave -divider out
add wave -position end sim:/fft_tb/dut/valid_out
add wave -position end -unsigned sim:/fft_tb/dut/addr
add wave -position end -decimal sim:/fft_tb/dut/d_out_re
add wave -position end -decimal sim:/fft_tb/dut/d_out_im

run -all
wave zoom full
