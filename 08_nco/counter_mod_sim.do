#-
# SPDX-License-Identifier: BSD-3-Clause
#
# Copyright (c) 2020 Rafal Kozik
# All rights reserved.
#

vlib work

vlog counter_mod.sv
vlog counter_mod_tb.sv

vsim -novopt work.counter_mod_tb

add wave -position end sim:/counter_mod_tb/clk
add wave -position end sim:/counter_mod_tb/rst
add wave -unsigned -position end sim:/counter_mod_tb/mod/data
add wave -unsigned -position end sim:/counter_mod_tb/dut/mod_pre
add wave -unsigned -position end sim:/counter_mod_tb/dut/mod_r
add wave -unsigned -position end sim:/counter_mod_tb/dut/q
add wave -format analog-step -unsigned -height 100 -min -0 -max 15 sim:/counter_mod_tb/dut/q
add wave -unsigned -position end sim:/counter_mod_tb/dut/ov

run -all
wave zoom full
