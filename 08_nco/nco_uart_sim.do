#-
# SPDX-License-Identifier: BSD-3-Clause
#
# Copyright (c) 2020 Rafal Kozik
# All rights reserved.
#

vlib work

vlog ../05_uart_tx/uart_pkg.sv
vlog ../02_counter/counter.sv
vlog ../05_uart_tx/uart_tx.sv
vlog ../06_uart_rx/uart_rx.sv
vlog ../07_pwm/pwm.sv
vlog counter_mod.sv
vlog sin_lut.sv
vlog nco.sv
vlog nco_uart.sv
vlog nco_uart_tb.sv

vsim -novopt work.nco_uart_tb

add wave -position end sim:/nco_uart_tb/clk
add wave -position end sim:/nco_uart_tb/rst
add wave -position end -unsigned sim:/nco_uart_tb/bus/data
add wave -position end sim:/nco_uart_tb/rtx
add wave -position end -unsigned sim:/nco_uart_tb/dut/bus_sin/data
add wave -position end -format analog-step -unsigned -height 100 -min 0 -max 127 sim:/nco_uart_tb/dut/bus_sin/data
add wave -position end sim:/nco_uart_tb/dut/pwma
add wave -position end sim:/nco_uart_tb/dut/pwmb

run -all
wave zoom full
