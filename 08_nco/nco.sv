/*-
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * Copyright (c) 2020 Rafal Kozik
 * All rights reserved.
 */

`default_nettype none

module nco #(
	parameter modN = 8,
	parameter L = 1024,
	parameter sinN = 10,
	parameter file_name="nco_lut.mem",
	parameter logL = $clog2(L)
) (
	input wire ce,
	StreamBus period,
	StreamBus out
);
	logic [logL-1:0] angle;
	logic angle_ce;

	counter_mod #(.N(modN)) ctx_mod (
		.mod(period),
		.ce(ce),
		.q(),
		.ov(angle_ce));

	counter #(.N(L)) ctx_angle (
		.clk(period.clk),
		.rst(period.rst),
		.ce(angle_ce & ce),
		.q(angle),
		.ov());

	sin_lut #(.N(sinN), .L(L), .file_name(file_name)) lut (
		.clk(period.clk),
		.angle(angle),
		.sinus(out.data));

	assign out.valid = 1'b1;

endmodule

`default_nettype wire
