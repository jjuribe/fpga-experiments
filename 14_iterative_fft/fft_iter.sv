/*-
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * Copyright (c) 2020 Rafal Kozik
 * All rights reserved.
 */

`default_nettype none

module fft_iter #(
    parameter K = 8,
    parameter N = 2,
    parameter LOG_N = $clog2(N),
    parameter LLOG_N = $clog2(LOG_N)
) (
    input wire clk,
    input wire rst,
    input wire signed [K-1:0]d_in_re,
    input wire signed [K-1:0]d_in_im,
    input wire [LOG_N-1:0] addr_in,
    input wire valid_in,
    input wire start,
    output logic ready,
    output logic signed [K-1:0]d_out_re,
    output logic signed [K-1:0]d_out_im,
    output logic [LOG_N-1:0] addr_out,
    output logic valid_out
);
    logic signed [K-1:0]d_re;
    logic signed [K-1:0]d_im;
    logic signed [K-1:0]d1_re;
    logic signed [K-1:0]d1_im;
    logic signed [K:0]add_re;
    logic signed [K:0]add_im;
    logic signed [K:0]add1_re;
    logic signed [K:0]add1_im;
    logic signed [K:0]sub_re;
    logic signed [K:0]sub_im;
    logic signed [K:0]W_re;
    logic signed [K:0]W_im;
    logic signed [K:0]c_re;
    logic signed [K:0]c_im;
    logic signed [K:0]c1_re;
    logic signed [K:0]c1_im;
    logic signed [K-1:0]out_re;
    logic signed [K-1:0]out_im;
    logic [LOG_N-1:0]i;
    logic [LLOG_N-1:0]n;
    logic sel, work, mod_N_ov, mod_lN_ov;
    logic start1, work1;
    logic [LOG_N-1:0]ram_addr;
    logic [LOG_N-1:0]ram_addr1;
    logic [LOG_N-1:0]w_addr;
    logic [LOG_N-1:0]w_addr1;

    always_ff @(posedge clk)
        if (!rst)
            start1 <= '0;
        else
            start1 <= start;
    
    always_ff @(posedge clk)
        if (!rst)
            work <= '0;
        else if (start != start1)
            work <= 1'b1;
        else if (work & mod_N_ov & mod_lN_ov)
            work <= 1'b0;

    assign ready = !(work | work1);

    counter #(.N(N)) mod_N (
        .clk(clk),
        .rst(rst),
        .ce(work),
        .q(i),
        .ov(mod_N_ov));

    counter #(.N(LOG_N)) mod_log_N (
        .clk(clk),
        .rst(rst),
        .ce(work & mod_N_ov),
        .q(n),
        .ov(mod_lN_ov));

    always_comb
        for (int ii = 0; ii < LOG_N; ii++)
            if (ii > LOG_N-1-n)
                ram_addr[ii] = i[ii];
            else if (ii == LOG_N-1-n)
                ram_addr[ii] = i[0];
            else if (ii < LOG_N-1)
                ram_addr[ii] = i[ii+1];
            else
                ram_addr[ii] = '0;

    assign w_addr = i[LOG_N-1:1] << n;

    delay #(.N(6), .L(LOG_N)) d_wr_addr (
        .clk(clk),
        .rst(1'b1),
        .ce(1'b1),
        .in(ram_addr),
        .out(ram_addr1));

    delay #(.N(6), .L(1)) d_work (
        .clk(clk),
        .rst(rst),
        .ce(1'b1),
        .in(work),
        .out(work1));

    ram #(.N(N), .K(2*K)) fft_ram (
        .clk(clk),
        .wr(work1 | valid_in),
        .addr_wr(work1 ? ram_addr1 : addr_in),
        .data_wr(work1 ? {out_re,out_im} : {d_in_re,d_in_im}),
        .addr_rd(ram_addr),
        .data_rd({d_re, d_im}));

    always_ff @(posedge clk)
        {d1_re, d1_im} <= {d_re, d_im};

    always_ff @(posedge clk) begin
        add_re <= d1_re + d_re;
        add_im <= d1_im + d_im;
        sub_re <= d1_re - d_re;
        sub_im <= d1_im - d_im;
    end

    delay #(.N(2), .L(2*(K+1))) d_add (
        .clk(clk),
        .rst(1'b1),
        .ce(1'b1),
        .in({add_re, add_im}),
        .out({add1_re, add1_im}));

    delay #(.N(2), .L(LOG_N)) d_w_addr (
        .clk(clk),
        .rst(1'b1),
        .ce(1'b1),
        .in(w_addr),
        .out(w_addr1));

    twiddle_lut #(.N(N), .K(K+1)) twiddle (
        .clk(clk),
        .i(w_addr1[LOG_N-2:0]),
        .W_re(W_re),
        .W_im(W_im));

    c_mul #(.K(K+1)) mul (
        .clk(clk),
        .rst(rst),
        .a_re(sub_re),
        .a_im(sub_im),
        .b_re(W_re),
        .b_im(W_im),
        .c_re(c_re),
        .c_im(c_im));

    assign {c1_re, c1_im} = {c_re, c_im};

    delay #(.N(6), .L(1)) d_sel (
        .clk(clk),
        .rst(1'b1),
        .ce(1'b1),
        .in(i[0]),
        .out(sel));

    assign {out_re, out_im} = sel ?
        {c1_re[K:1], c1_im[K:1]} :
        {add1_re[K:1], add1_im[K:1]};

    assign {d_out_re, d_out_im} = {out_re, out_im};
    genvar j;
    generate
        // assign addr_out = {<<{ram_addr1}}; not supported
        for (j = 0; j < $bits(addr_out); j++) begin : BIT_REV
            assign addr_out[j] = ram_addr1[$bits(addr_out) - j - 1];
        end
    endgenerate 

    delay #(.N(6), .L(1)) d_valid (
        .clk(clk),
        .rst(1'b1),
        .ce(1'b1),
        .in(n == (LOG_N-1)),
        .out(valid_out));

endmodule

`default_nettype wire
