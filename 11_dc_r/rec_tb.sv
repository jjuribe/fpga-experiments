/*-
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * Copyright (c) 2020 Rafal Kozik
 * All rights reserved.
 */

`default_nettype none
`timescale 100ns / 10ns

module rec_tb;
    logic clk, rst;
    logic [11:0]adc_data_out;
    logic signed [9:0]data_dc;
    logic signed [7:0]data_out;

    initial begin
		clk <= 1'b0;
		forever #62.5ns clk <= ~clk;
	end

	initial begin
		rst <= 1'b0;
        #1250 rst <= 1'b1;
	end

    rec dut (
        .clk(clk),
        .rst(rst),
        .tx(),
        .led());

    always_ff @(posedge clk) begin
        if (dut.adc_valid_out)
            adc_data_out <= dut.adc_data_out;
        if (dut.bus_dc.valid)
            data_dc <= dut.bus_dc.data;
        if (dut.bus_uart.valid)
            data_out <= dut.bus_uart.data;
    end

endmodule

`default_nettype wire
