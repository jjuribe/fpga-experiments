/*-
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * Copyright (c) 2020 Rafal Kozik
 * All rights reserved.
 */

`default_nettype none

module saturation #(
    parameter N_IN = 10,
    parameter N_OUT = 8
) (
	StreamBus in,
	StreamBus out
);
    logic overflow, sign;
    logic [N_IN-1:N_OUT-1]truncated;
    logic [N_OUT-1:0]data_out;

    always_comb begin
        sign = in.data[N_IN-1];
        truncated = in.data[N_IN-1:N_OUT-1];
        overflow = &truncated != |truncated;
        if (overflow)
            data_out = (sign) ? 2**(N_OUT-1) : 2**(N_OUT-1)-1;
        else
            data_out = in.data[N_OUT-1:0];
    end

    always_ff @(posedge in.clk)
            out.data <= data_out;

    always_ff @(posedge in.clk or negedge in.rst)
    if (!in.rst)
        out.valid <= '0;
    else
        out.valid <= in.valid;

endmodule

`default_nettype wire
