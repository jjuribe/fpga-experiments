vlib work

vlog truncation_saturation.sv

vsim -novopt work.truncation_saturation

add wave -position end -format analog-step -decimal -height 100 -min -128 -max 127 sim:/truncation_saturation/data
add wave -position end -format analog-step -decimal -height 100 -min -128 -max 127 sim:/truncation_saturation/data_t
add wave -position end -format analog-step -decimal -height 100 -min -128 -max 127 sim:/truncation_saturation/data_s

run -all
wave zoom full
