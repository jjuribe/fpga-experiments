#-
# SPDX-License-Identifier: BSD-3-Clause
#
# Copyright (c) 2020 Rafal Kozik
# All rights reserved.
#

vlib work

vlog ../05_uart_tx/uart_pkg.sv
vlog saturation.sv
vlog saturation_tb.sv

vsim -novopt work.saturation_tb

add wave -position end sim:/saturation_tb/clk
add wave -position end sim:/saturation_tb/rst
add wave -position end sim:/saturation_tb/in/valid
add wave -position end -format analog-step -signed -height 100 -min -512 -max 511 sim:/saturation_tb/in/data
add wave -position end -signed sim:/saturation_tb/in/data

add wave -position end sim:/saturation_tb/dut/sign
add wave -position end -binary sim:/saturation_tb/dut/truncated
add wave -position end sim:/saturation_tb/dut/overflow

add wave -position end -decimal sim:/saturation_tb/dut/data_out
add wave -position end -format analog-step -decimal -height 100 -min -128 -max 127 sim:/saturation_tb/dut/data_out

add wave -position end sim:/saturation_tb/out/valid
add wave -position end -decimal sim:/saturation_tb/out/data
add wave -position end -format analog-step -decimal -height 100 -min -128 -max 127 sim:/saturation_tb/out/data

run -all
wave zoom full
