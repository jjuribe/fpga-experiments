/*-
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * Copyright (c) 2020 Rafal Kozik
 * All rights reserved.
 */

`default_nettype none

module dc_r #(
    parameter N = 8,
    parameter S = 5
) (
	StreamBus in,
	StreamBus out
);
    parameter L = N+S;
    logic [2:0]valid_d;
    logic signed [N:0]x;
    logic signed [L:0]s;
    logic signed [L:0]sum;
    logic signed [L:0]s_delay;
    logic signed [L:0]sum_delay;
    logic signed [L+1:0]y;

    always_ff @(posedge in.clk or negedge in.rst)
        if (!in.rst)
            valid_d <= '0;
        else
            valid_d <= {valid_d[1:0], in.valid};

    always_ff @(posedge in.clk)
        x <= {1'd0, in.data};

    assign sum = s - (s >>> S) + x;

    always_ff @(posedge in.clk or negedge in.rst)
        if (!in.rst)
            s <= '0;
        else if (valid_d[0])
            s <= sum[L:0];

    always_ff @(posedge in.clk) begin
        s_delay <= s;
        sum_delay <= sum[L:0];
    end

    always_ff @(posedge in.clk)
        y <= sum_delay - s_delay;

    assign out.data = y[N-1:0];
    assign out.valid = valid_d[2];
endmodule

`default_nettype wire
