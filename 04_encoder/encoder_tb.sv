/*-
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * Copyright (c) 2019 Rafal Kozik
 * All rights reserved.
 */

`default_nettype none
`timescale 100ns / 10ns

module encoder_tb;
	logic clk, rst, a, b;

	initial begin
		clk <= '0;
		forever #62.5ns clk = ~clk;
	end

	initial begin
		rst <= 1'b0;
		#125ns rst <= 1'b1;
	end

	initial begin
		a <= 1'b0;
		b <= 1'b1;
		repeat (10) begin
			#10ms a <= 1'b1;
			#10ms b <= 1'b0;
			#10ms a <= 1'b0;
			#10ms b <= 1'b1;
		end
		repeat (10) begin
			#10ms b <= 1'b0;
			#10ms a <= 1'b1;
			#10ms b <= 1'b1;
			#10ms a <= 1'b0;
		end
		$stop;
	end

	encoder #(
		.LED_N(8),
		.DEBONUCE_N(14)
	) dut (
		.clk(clk),
		.rst(rst),
		.a(a),
		.b(b),
		.led()
	);
endmodule

`default_nettype wire
