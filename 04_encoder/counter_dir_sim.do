#-
# SPDX-License-Identifier: BSD-3-Clause
#
# Copyright (c) 2019 Rafal Kozik
# All rights reserved.
#

vlib work

vlog counter_dir.sv
vlog counter_dir_tb.sv

vsim -novopt work.counter_dir_tb

add wave -position end sim:/counter_dir_tb/clk
add wave -position end sim:/counter_dir_tb/rst
add wave -position end sim:/counter_dir_tb/ce
add wave -position end sim:/counter_dir_tb/dir
add wave -position end -unsigned sim:/counter_dir_tb/dut/q

run -all
wave zoom full
