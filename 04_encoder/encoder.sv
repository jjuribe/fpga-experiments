/*-
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * Copyright (c) 2019 Rafal Kozik
 * All rights reserved.
 */

`default_nettype none

module encoder #(
	parameter LED_N = 8,
	parameter DEBONUCE_N = 14
) (
	input wire clk,
	input wire rst,
	input wire a,
	input wire b,
	output logic [LED_N-1:0]led
);
	logic a_r, b_r;
	logic [LED_N-1:0]led_out;
	logic a_d, a_e, b_d;

	always_ff @(posedge clk or negedge rst)
		if (!rst) begin
			a_r <= '0;
			b_r <= '0;
			led <= '0;
		end else begin
			a_r <= a;
			b_r <= b;
			led <= led_out;
		end

	debounce #(.N(DEBONUCE_N)) db_a (
		.clk(clk),
		.rst(rst),
		.d(a_r),
		.q(a_d)
	);

	debounce #(.N(DEBONUCE_N)) db_b (
		.clk(clk),
		.rst(rst),
		.d(b_r),
		.q(b_d)
	);

	edge_detector ed_a (
		.clk(clk),
		.rst(rst),
		.d(a_d),
		.q(a_e)
	);

	counter_dir #(.N(LED_N)) dut (
		.clk(clk),
		.rst(rst),
		.ce(a_e),
		.dir(b_d),
		.q(led_out)
	);
endmodule

`default_nettype wire
