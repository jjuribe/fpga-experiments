/*-
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * Copyright (c) 2019 Rafal Kozik
 * All rights reserved.
 */

`default_nettype none
`timescale 100ns / 10ns

module counter_dir_tb;
	logic clk, rst, ce, dir;

	initial begin
		clk <= '0;
		forever #5 clk <= ~clk;
	end

	initial begin
		rst <= 1'b0;
		#10 rst <= 1'b1;
	end

	initial begin
		dir <= 1'b1;
		ce <= 1'b1;
		repeat (10) @(posedge clk);
		ce <= 1'b0;
		repeat (5) @(posedge clk);
		ce <= 1'b1;
		dir <= 1'b0;
		repeat (10) @(posedge clk);
		$stop;
	end

	counter_dir #(.N(3)) dut (
		.clk(clk),
		.rst(rst),
		.ce(ce),
		.dir(dir),
		.q()
	);
endmodule

`default_nettype wire
